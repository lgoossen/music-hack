//
//  SoundGenerator.swift
//  SwiftSimpleAUGraph
//
//  Created by Gene De Lisa on 6/8/14.
//  Copyright (c) 2014 Gene De Lisa. All rights reserved.
//

import Foundation
import AudioToolbox
import AVFoundation
import Accelerate

//import CoreAudio

class AudioEngine : NSObject {
	var processingGraph = AUGraph()
    var samplerNode = AUNode()
	var mainMixerNode = AUNode()
    var ioNode = AUNode()
    var samplerUnit = AudioUnit()
	var	mainMixerUnit = AudioUnit()
    var ioUnit = AudioUnit()
    var isPlaying = false
	let session = AVAudioSession.sharedInstance()
	//let functionHelper = CallBackFunctionHelper()
	
	//Trying to Test channel splitting
	//var channelSplitterInfo = InfoStruct()

	
	// MARK: AudioComponentDescriptions
	
	// IO
	var ioUnitComponentDescription = AudioComponentDescription(componentType: OSType(kAudioUnitType_Output),componentSubType: OSType(kAudioUnitSubType_RemoteIO),componentManufacturer: OSType(kAudioUnitManufacturer_Apple),componentFlags: 0,componentFlagsMask: 0)
	
	// Regular Mixer
	var mixerComponentDescription = AudioComponentDescription(componentType: OSType(kAudioUnitType_Mixer),componentSubType: OSType(kAudioUnitSubType_MultiChannelMixer),componentManufacturer: OSType(kAudioUnitManufacturer_Apple),componentFlags: 0,componentFlagsMask: 0)
	
	// Sampler
	var samplerComponentDescription = AudioComponentDescription(componentType: OSType(kAudioUnitType_MusicDevice),componentSubType: OSType(kAudioUnitSubType_Sampler),componentManufacturer: OSType(kAudioUnitManufacturer_Apple),componentFlags: 0,componentFlagsMask: 0)

	// MARK: Regular Fuctions
    override init() {
		super.init()
		initAVAudioSession()
        augraphSetup()
        graphStart()
		println("The mixer output info\(howManyChannelsDoesTheMixerHave())")
		
		change(0, toVol: 1.0)
		change(1, toVol: 0.0)
		//change(2, toVol: 0.0)
		//change(3, toVol: 0.0)
		//change(4, toVol: 0.0)
		//change(5, toVol: 0.0)
		//change(6, toVol: 0.0)
		//change(7, toVol: 0.0)
		//change(8, toVol: 0.0)
		//playNoteOn(60, velocity: 100)
		
		
		
		//var rendercallback = Rendertone()
/*
		let p = UnsafeMutablePointer<() -> String>.alloc(1) // allocate memory for function
		p.initialize(RenderTone) // initialize with value
		
		let cp = COpaquePointer(p) // convert UnsafeMutablePointer to COpaquePointer
		let fp = CFunctionPointer<() -> String>(cp) // convert COpaquePointer to CFunctionPointer
*/
		//		var callback:AURenderCallbackStruct = AURenderCallbackStruct(inputProc: , inputProcRefCon: <#UnsafeMutablePointer<Void>#>)//AURenderCallbackStruct(RenderTone,nil)

    }
    
    
    func augraphSetup() {
        var status : OSStatus = 0
        status = NewAUGraph(&processingGraph)
        CheckError(status)
        
        // create the sampler
        // for now, just have it play the default sine tone
        //https://developer.apple.com/library/prerelease/ios/documentation/AudioUnit/Reference/AudioComponentServicesReference/index.html#//apple_ref/swift/struct/AudioComponentDescription
        
		
        status = AUGraphAddNode(processingGraph, &samplerComponentDescription, &samplerNode)
        CheckError(status)
        
        status = AUGraphAddNode(processingGraph, &ioUnitComponentDescription, &ioNode)
        CheckError(status)
		
		//mixer
		status = AUGraphAddNode(processingGraph, &mixerComponentDescription, &mainMixerNode)
		CheckError(status)

        // now do the wiring. The graph needs to be open before you call AUGraphNodeInfo
        status = AUGraphOpen(self.processingGraph)
        CheckError(status)
        status = AUGraphNodeInfo(self.processingGraph, self.samplerNode, nil, &samplerUnit)
        CheckError(status)
        status = AUGraphNodeInfo(self.processingGraph, self.ioNode, nil, &ioUnit)
        CheckError(status)
		// mixer 
		status = AUGraphNodeInfo(processingGraph, mainMixerNode, nil, &mainMixerUnit)
		
		//Enable Input
		var enableInput:UInt32 = 1;    // to enable input
		//let propertyID:AudioUnitPropertyID = kAudioOutputUnitProperty_EnableIO
		
		status = AudioUnitSetProperty(ioUnit, AudioUnitPropertyID(kAudioOutputUnitProperty_EnableIO), AudioUnitScope(kAudioUnitScope_Input), 1, &enableInput, UInt32(sizeof(UInt32)))
		CheckError(status)
		
		//var ioUnitOutputElement:AudioUnitElement = 0
		//var ioUnitInputElement:AudioUnitElement = 1
		//var samplerOutputElement:AudioUnitElement = 0
		
		// Get ASBD From IO
		var ASBD = AudioStreamBasicDescription()
		var sizeOfASBD = UInt32(sizeof(AudioStreamBasicDescription))
		status = AudioUnitGetProperty(samplerUnit, AudioUnitPropertyID(kAudioUnitProperty_StreamFormat), AudioUnitPropertyID(kAudioUnitScope_Output), 0, &ASBD, &sizeOfASBD)
		CheckError(status)
		
		
/*
		status = AudioUnitGetProperty(ioUnit,
			kAudioOutputUnitProperty_ChannelMap,
			kAudioUnitScope_Output,
			0,
			channelMapPtr,
			&scratch);
*/
		
		
		
		println("this is the ASBD:\(ASBD.mChannelsPerFrame)")
		ASBD.mChannelsPerFrame = 8
		
		status = AudioUnitSetProperty(ioUnit, AudioUnitPropertyID(kAudioUnitProperty_StreamFormat), AudioUnitPropertyID(kAudioUnitScope_Output), 1, &ASBD, sizeOfASBD)
		CheckError(status)
		println("this is the ASBD:\(ASBD.mChannelsPerFrame)")
		
		//Trying to Test channel splitting
		//channelSplitterInfo = InfoStruct(bufferList:AudioBufferList(), inputUnit:ioUnit, busCounter:0, numberOfChannels:0)

		//main mixer to output
		status = AUGraphConnectNodeInput(self.processingGraph,
			mainMixerNode, 0, // srcnode, inSourceOutputNumber
			ioNode, 0) // destnode, inDestInputNumber
		CheckError(status)
		
		//sampleer to mixer
		status = AUGraphConnectNodeInput(self.processingGraph,
			samplerNode, 0, // srcnode, inSourceOutputNumber
			mainMixerNode, 0) // destnode, inDestInputNumber
		CheckError(status)
		//		status = AUGraphConnectNodeInput(processingGraph, ioNode, 1, mainMixerNode, 0)
		//CheckError(status)
		
		// add callback to mixer
		//withUnsafeMutablePointer(&ioUnit) {ptr -> Void in
		//	addInputCallbackTo(mainMixerNode, elementNumber: 0, helperFunc: functionHelper.passThru(ptr))}
		
		// Trying to set mixer input bus number. This is a bug in the mixer. This doesnt change to anything below 8. http://stackoverflow.com/a/19330133/1287308
		
		ASBD.mChannelsPerFrame = 1
		status = AudioUnitSetProperty(mainMixerUnit, AudioUnitPropertyID(kAudioUnitProperty_StreamFormat), AudioUnitPropertyID(kAudioUnitScope_Input), 1, &ASBD, sizeOfASBD)
		CheckError(status)


		//addInputCallbackTo(mainMixerNode, elementNumber: 1, helperFunc: functionHelper.filterCallback())
	}
	
    func graphStart() {
        //https://developer.apple.com/library/prerelease/ios/documentation/AudioToolbox/Reference/AUGraphServicesReference/index.html#//apple_ref/c/func/AUGraphIsInitialized
        
        var status : OSStatus = OSStatus(noErr)
        var inoutIsInitialized:Boolean = 0
        status = AUGraphIsInitialized(self.processingGraph, &inoutIsInitialized)
        CheckError(status)
        if inoutIsInitialized == 0 {
			println("Graph was not initialized... initializing now")
            status = AUGraphInitialize(self.processingGraph)
            CheckError(status)
        }
        
        var isRunning:Boolean = 0
        AUGraphIsRunning(self.processingGraph, &isRunning)
        if isRunning == 0 {
			println("Graph was not running... starting now")
            status = AUGraphStart(self.processingGraph)
            CheckError(status)
        }
        
        self.isPlaying = true;
    }
    
    func playNoteOn(noteNum:UInt32, velocity:UInt32)    {
        var noteCommand:UInt32 = 0x90 | 0;
        var status : OSStatus = OSStatus(noErr)
        status = MusicDeviceMIDIEvent(self.samplerUnit, noteCommand, noteNum, velocity, 0)
        CheckError(status)
        println("noteon status is \(status)")
    }
    
    func playNoteOff(noteNum:UInt32)    {
        var noteCommand:UInt32 = 0x80 | 0;
        var status : OSStatus = OSStatus(noErr)
        status = MusicDeviceMIDIEvent(self.samplerUnit, noteCommand, noteNum, 0, 0)
        CheckError(status)
        println("noteoff status is \(status)")
    }
	
	func howManyChannelsDoesTheMixerHave()->UInt32{
		var status : OSStatus = OSStatus(noErr)
		var elementCount = UInt32()
		var sizeOfElementCount = UInt32(sizeof(UInt32))
		status = AudioUnitGetProperty(mainMixerUnit, AudioUnitPropertyID(kAudioUnitProperty_ElementCount), AudioUnitPropertyID(kAudioUnitScope_Input), 0, &elementCount, &sizeOfElementCount)
		CheckError(status)
		return elementCount

	}
	
	func change(channel:UInt32, toVol:Float32){
		var status : OSStatus = OSStatus(noErr)
		var channelNumebr = channel
		var vol = toVol
		let inframes:UInt32 = 0
		status = AudioUnitSetParameter(mainMixerUnit, AudioUnitParameterID(kMultiChannelMixerParam_Volume), AudioUnitPropertyID(kAudioUnitScope_Input), channelNumebr, vol, inframes)
		CheckError(status)
	}
	
	// MARK: Swift Wrappers
	
	func addInputCallbackTo(AUNode, elementNumber:Int, helperFunc:AURenderCallbackStruct){
		var auStruct = helperFunc
		let destInputNumber = UInt32(elementNumber)
		withUnsafePointer(&auStruct) {ptr -> Void in
			AUGraphSetNodeInputCallback(self.processingGraph, self.mainMixerNode, destInputNumber, ptr)}
	}

	// MARK: AVAudioSession
	
	func initAVAudioSession(){
		// For complete details regarding the use of AVAudioSession see the AVAudioSession Programming Guide
		// https://developer.apple.com/library/ios/documentation/Audio/Conceptual/AudioSessionProgrammingGuide/Introduction/Introduction.html
		
		// Configure the audio session

		let audioSession = AVAudioSession.sharedInstance()
		
		// set the session category
		let error = NSErrorPointer()
		var success = audioSession.setCategory(AVAudioSessionCategoryMultiRoute, error: error)
		if (!success){println("Error setting AVAudioSession category! \(error.debugDescription)")}
		
		// set the sample rate
		let preferredHWSampleRate = 44100.0
		success = audioSession.setPreferredSampleRate(preferredHWSampleRate, error: error)
		if (!success){println("Error setting preferred sample rate! \(error.debugDescription)")}
		
		// set the buffer Durration
		let preferredBufferDurration = 0.00001 //seconds
		success = audioSession.setPreferredIOBufferDuration(preferredBufferDurration, error: error)
		if (!success){println("Error setting preferred io buffer duration! \(error.debugDescription)")}
		
		// add interruption handler
		NSNotificationCenter.defaultCenter().addObserver(self, selector: "handleInterruption", name: AVAudioSessionInterruptionNotification, object: audioSession)
		
		// Route Change handler
		NSNotificationCenter.defaultCenter().addObserver(self, selector: "handleRouteChange:", name: AVAudioSessionRouteChangeNotification, object: audioSession)
		
		// Media Services Reset
		NSNotificationCenter.defaultCenter().addObserver(self, selector: "handleMediaServicesReset", name: AVAudioSessionMediaServicesWereResetNotification, object: audioSession)
		
		// Media Services Were Lost
		NSNotificationCenter.defaultCenter().addObserver(self, selector: "handleMediaServicesLost", name: AVAudioSessionMediaServicesWereLostNotification, object: audioSession)
		
		// activate the audio session
		success = audioSession.setActive(true, error: error)
		if (!success){println("Error setting session active! \(error.debugDescription)")}
		
		println("current route\(audioSession.currentRoute)")
		println("Audio Session inputNumberOfChannels \(audioSession.inputNumberOfChannels)")


		
	}
	
	func handleInterruption(notification:NSNotification) {
		
		if notification.name != AVAudioSessionInterruptionNotification
			|| notification.userInfo == nil{
				return
		}
		var info = notification.userInfo!
		var intValue: UInt = 0
		(info[AVAudioSessionInterruptionTypeKey] as! NSValue).getValue(&intValue)
		println("Session interrupted > --- ")
		if let type = AVAudioSessionInterruptionType(rawValue: intValue) {
			switch type {
			case .Began:
				// interruption began
				println("Begin Interruption")
			case .Ended:
				// interruption ended
				println("End Interruption")
			}
		}
	}
	
	func handleRouteChange(notification:NSNotification) {
		if notification.name == AVAudioSessionRouteChangeNotification && notification.userInfo != nil{
			println("Route change:")
			let info = notification.userInfo!
			var intValue: UInt = 0
			(info[AVAudioSessionRouteChangeReasonKey] as! NSValue).getValue(&intValue)
			
			if let changeReason = AVAudioSessionRouteChangeReason(rawValue: intValue){
				switch (changeReason) {
				case .NewDeviceAvailable:
					println("NewDeviceAvailable")
					break
				case .OldDeviceUnavailable:
					println("OldDeviceUnavailable")
					break
				case .CategoryChange:
					println("CategoryChange")
					println("New Category: %@", session.category)
					break
				case .Override:
					println("     Override")
					break
				case .WakeFromSleep:
					println("WakeFromSleep")
					break
				case .NoSuitableRouteForCategory:
					println("NoSuitableRouteForCategory")
					break
				case .RouteConfigurationChange:
					println("RouteConfigurationChange")
					break
				default:
					println("ReasonUnknown");
				}
			}
			if let routeDescription = info[AVAudioSessionRouteChangePreviousRouteKey] as? AVAudioSessionRouteDescription{
				println("Previous route:\(routeDescription)")
			}
			println("current route\(AVAudioSession.sharedInstance().currentRoute.outputs[0].channels)")
		}
	}
	
	func handleMediaServicesReset(notification:NSNotification) {
		// if we've received this notification, the media server has been reset
		// re-wire all the connections and start the engine
		println("Media services have been reset!")
		println("Re-wiring connections and starting once again")
		
		augraphSetup()
		graphStart()
	}
	
	func handleMediaServicesLost(notification:NSNotification) {
		//TUDO
		println("MediaServicesLost")
	}
}
