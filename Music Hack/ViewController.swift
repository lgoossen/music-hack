//
//  ViewController.swift
//  Music Hack
//
//  Created by Lucas Goossen on 6/13/15.
//  Copyright (c) 2015 Lucas Goossen. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
	
	let engine = SoundGenerator()
    var lastValue: Int = 6
    var noteLabels: Array<String> = Array(arrayLiteral: "C", "D", "E", "F", "G", "A", "B", "C", "D", "E", "F", "G")

	override func viewDidLoad() {
		super.viewDidLoad()
	}

	override func didReceiveMemoryWarning() {
		super.didReceiveMemoryWarning()
		// Dispose of any resources that can be recreated.
	}
	
	func playLastNote(note:Int){
		engine.musicSequence = engine.createMusicSequence([note], beatlLength: [1.0])
		engine.musicPlayer = engine.createPlayer(engine.musicSequence)
		engine.play()
		
	}
	
	@IBAction func startPlayingNotes(sender: AnyObject) {
		engine.musicSequence = engine.createMusicSequence([7,4,5,6,lastValue], beatlLength: [1.0, 1.0, 1.0, 1.0, 1.0])
		engine.musicPlayer = engine.createPlayer(engine.musicSequence)
		engine.play()

	}

	@IBAction func stopPlayingNotes(sender: AnyObject) {
		engine.playNoteOff(60)
	}
    
    @IBAction func submitAnswer(sender: AnyObject) {
        startPlayingNotes(sender)
        if (lastValue == 7) {
            lastNote.image = lastNote.image!.imageWithRenderingMode(UIImageRenderingMode.AlwaysTemplate)
            lastNote.tintColor = UIColor.greenColor()
        }
        else {
            lastNote.image = lastNote.image!.imageWithRenderingMode(UIImageRenderingMode.AlwaysTemplate)
            lastNote.tintColor = UIColor.redColor()        }
    }
    
    @IBOutlet weak var resultLabel: UILabel!
    @IBOutlet weak var lastNote: UIImageView!
    
    @IBAction func adjustNote(sender: UIStepper) {
        var noteIndex: Int = Int(sender.value)
        var fromPoint: CGPoint = lastNote.layer.position
        var toPoint: CGPoint
        
        if (lastValue < noteIndex) {
            toPoint = CGPoint(x: fromPoint.x, y: fromPoint.y - lastNote.frame.height / 2)
        } else {
            toPoint = CGPoint(x: fromPoint.x, y: fromPoint.y + lastNote.frame.height / 2)
        }
        lastValue = noteIndex
        
        var movement = CABasicAnimation(keyPath: "position")
        movement.additive = true
        movement.fromValue =  NSValue(CGPoint: fromPoint)
        movement.toValue =  NSValue(CGPoint: toPoint)
        movement.duration = 0.3
        
        lastNote.layer.addAnimation(movement, forKey: "basic")
        lastNote.layer.position = toPoint
        lastNote.image = lastNote.image!.imageWithRenderingMode(UIImageRenderingMode.AlwaysTemplate)
        lastNote.tintColor = UIColor.blackColor()

		// play note
        playLastNote(noteIndex)
        // Update label
    }
}

